<?php
include "functions.php";
$category = \Classes\Category::find($_GET['id']);
?>
<html lang="en">
<?php include "head.php"; ?>
<body>
<div class="container background">
    <?php include "header.php"; ?>
    <div class="row page">
        <?php foreach ($category->getProducts() as $product): ?>
            <div class="col-4 ">
                <div class="card stylecard" style="width: 20rem;">
                    <img class="afis" src="images/<?php echo $product->image; ?>" class="card-img-top" alt="<?php echo $product->name; ?>">
                    <div class="card-body">
                        <h6 class="card-title"><?php echo $product->name; ?></h6>
                        <p class="card-text descript"><?php echo $product->description; ?></p>
                        <p class="card-text cod"><?php echo $product->code; ?></p>
                        <p class="card-text pret"><?php echo $product->price; ?> RON </p>
                        <a href="#" class="btn btn-info cos">Adauga in cos</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</body>
</html>
