
<div class="row page">
    <div class="col-12">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="product.php">Home</a>
        </li>
    <?php foreach (\Classes\Category::findBy('parent_id',0)as $mainCategory): ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="category.php?id=<?php echo $mainCategory->getID(); ?>" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $mainCategory->name?></a>
            <div class="dropdown-menu">
                <?php foreach ($mainCategory->getChildren()as $child): ?>
                    <a class="dropdown-item" href="category.php?id=<?php echo $child->getID(); ?>"><?php echo $child->name; ?></a>
            <?php endforeach; ?>
            </div>
        </li>
    <?php endforeach; ?>
        <li class="nav-item">
            <a class="nav-link " href="cart.php">Cart <span id="myCartTotalProduct" class="badge badge-danger mydangerbut"><?php echo getCurrentCart()->getTotalProducts(); ?></span></a>
        </li>
    </ul>
    </div>
</div>