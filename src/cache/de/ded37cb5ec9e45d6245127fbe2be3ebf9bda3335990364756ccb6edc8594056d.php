<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* product.html.twig */
class __TwigTemplate_55227b98b89273eb1497a67199f3ca5dfc8d1300e569691825d4467c274f76c5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
<html lang=\"en\">
<?php include \"head.php\"; ?>
<body>
<div class=\"container background\">
    <?php include \"header.php\"; ?>
    <div class=\"row page\">
            <?php foreach (Product::findAll()as \$product): ?>
                <div class=\"col-4 \">
                    <div class=\"card stylecard\" style=\"width: 20rem;\">
                        <img class=\"afis\" src=\"images/<?php echo \$product->image; ?>\"class=\"card-img-top\" alt=\"<?php echo \$product->name; ?>\">
                        <div class=\"card-body\">
                            <h6 class=\"card-title\"><?php echo \$product->name; ?></h6>
                            <p class=\"card-text descript\"><?php echo \$product->description; ?></p>
                            <p class=\"card-text cod\"><?php echo \$product->code; ?>
                            <div class=\"btn-group\">
                                <button class=\"btn btn-light btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                    Choose light color
                                </button>
                                <div class=\"dropdown-menu color\">
                                    <?php foreach (\$product->getColors() as \$productColor):?>
                                    <input type=\"checkbox\"/> <?php echo \$productColor->getColor()->name; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <p class=\"card-text pret\"><?php echo \$product->price; ?> RON </p>
                            <button data-url=\"addToCart.php?id=<?php echo \$product->getId(); ?>\" class=\"btn btn-info myAddToCart cos\">Adauga in cos</button>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "product.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "product.html.twig", "/var/www/html/national01/andreea/lights/src/templates/product.html.twig");
    }
}
