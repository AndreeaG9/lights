<?php
spl_autoload_register(function ($class_name) {
    include str_replace('\\','/',$class_name) . '.php';
});

require __DIR__ . '/vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(__DIR__ . "/src/Entities");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'Sco@l@it123',
    'dbname'   => 'national-01-andreea-lights-doctrine',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
$conn = \Doctrine\DBAL\DriverManager::getConnection($dbParams);


// obtaining the entity manager
$doctrine = EntityManager::create($conn, $config);

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . "/src/templates");
$twig = new \Twig\Environment($loader, [
    'cache' => __DIR__ . "/src/cache",
    'debug' => true
]);

$salt = 'Mys3cr3ts@lt';
session_start();
$conn = mysqli_connect('127.0.0.1','root','Sco@l@it123','national-01-andreea-lights');

function runQuery($sql){
    global $conn;
    $query = mysqli_query($conn, $sql);

    if (!$query){
        die("Mysql error on query:$sql - ".mysqli_error($conn));
    }
    if (is_bool($query)){
        return mysqli_insert_id($conn);
    } else {
        return $query->fetch_all(MYSQLI_ASSOC);
    }
}


function checkLogin(){
    if (!isset($_SESSION['user_id'])){
        header('Location: login.php');
        die;
    }
}
function getCurrentCart(){
    if(isset($_SESSION['cart_id'])){
        return \Classes\Cart::find($_SESSION['cart_id']);
    }else{
        $cart = new \Classes\Cart();
        $cart->save();
        $_SESSION['cart_id'] = $cart->getId();
        return $cart;
    }


}


