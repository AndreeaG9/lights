<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31.03.2021
 * Time: 15:40
 */
class Product extends BaseClass
{

    public $name;

    public $description;

    public $image;

    public $price;

    public $code;

    public $category_id;

    public $brand_id;

    public function getColors()
    {
        return ProductColor::findBy('product_id',$this->getId());
    }


    public function getBrand()
    {
        return Brand::find($this->brand_id);
    }

    public static function getTableName()
    {
        return 'products';
    }
}