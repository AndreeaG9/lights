<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.04.2021
 * Time: 14:20
 */
class Color extends Product
{


    /**
     * @return mixed
     */
    public function getName($parent=null)
    {
        $parentName = $parent->getName();
        return $this->name."($parentName)";
    }
}