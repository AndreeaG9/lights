<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.04.2021
 * Time: 11:30
 */
class ProductColor extends BaseClass
{

    public $product_id;

    public $color_id;

    public function getColor()
    {
        return Color::find($this->color_id);
    }

    public static function getTableName()
    {
        return 'product_colors';
    }

}