<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31.03.2021
 * Time: 23:10
 */
class CartProduct extends BaseClass
{

    public $product_id;

    public $quantity;

    public $cart_id;

    public $color_id;

    public function getProduct()
    {
        return Product::find($this->product_id);
    }

    public function getCart()
    {
        return Cart::find($this->cart_id);
    }

    public function getCartColor()
    {
        return ProductColor::find(12);
    }


    public function toJSON()
    {
        $data = [
            'id'=> $this->getId(),
            'lineTotal'=> $this->getTotal(),
            'cartTotal'=> $this->getCart()->getTotal(),
            'cartTotalProducts' => $this->getCart()->getTotalProducts()
        ];
        return json_encode($data);

    }

    public function getTotal()
    {
        return round($this->quantity * $this->getProduct()->price,2);
    }

    public static function getTableName()
    {
        return 'cart_products';
    }
}