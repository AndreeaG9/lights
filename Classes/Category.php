<?php
namespace Classes;
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31.03.2021
 * Time: 15:40
 */
class Category extends BaseClass
{

    public $name;

    public $parent_id;

    public function getParent()
    {
        return Category::find($this->parent_id);
    }

    public function getChildren()
    {
        return Category::findBy('parent_id',$this->getId());
    }

    public function getProducts()
    {
        $products = Product::findBy('category_id',$this->getId());

        foreach ($this->getChildren()as $child){
            $products = array_merge($products, $child->getProducts());
        }
        return $products;
    }

    public static function getTableName()
    {
        return 'categories';
    }
}