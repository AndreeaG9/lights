$(document).ready(function(){

    $('.myAddToCart').click(function () {
        var url = ($(this).data('url'));

        $.get(url, function (data) {
            $('#myCartTotalProduct').html(data);
        });

    });

    $('.myDeleteButton').click(function (){
        var url = $(this).data('url');

        $.get(url, function (data){
            var jsonData = JSON.parse(data);
            $('#myCartTotal').html(jsonData.cartTotal);
            $('#myCartTotalProduct').html(jsonData.cartTotalProducts);
        });
        $(this).parent().parent().remove();
    });

    $('.cartProductQuantity').change(function(){
        var url = $(this).data('url');

        $.post(url, {quantity:$(this).val()}, function(data){
            var jsonData = JSON.parse(data);
            $('#lineTotal'+jsonData.id).html(jsonData.lineTotal);
            $('#myCartTotal').html(jsonData.cartTotal);
            $('#myCartTotalProduct').html(jsonData.cartTotalProducts);
        });

        });
    });



